use std::time::Instant;

#[derive(Debug)]
struct Packet {
    hexbits: Vec<u64>,
    pos: usize,
}

impl Packet {
    fn nibble(&mut self, n: usize) -> &[u64] {
        self.pos += n;
        &self.hexbits[self.pos - n..self.pos]
    }
}

fn binary(thing: &[u64]) -> u64 {
    thing.iter().fold(0, |a, b| a << 1 | b)
}

fn parse_packet(bits: &mut Packet) -> (u64, u64) {
    let mut version = binary(bits.nibble(3));
    let type_id = binary(bits.nibble(3));
    if type_id == 4 {
        let mut n = 0;
        loop {
            let nibbled = bits.nibble(5);
            n = n << 4 | binary(&nibbled[1..]);
            if nibbled[0] == 0 {
                return (version, n);
            }
        }
    }

    let mut val = Vec::new();

    if bits.nibble(1)[0] == 0 {
        let n = binary(bits.nibble(15)) as usize + bits.pos;
        while bits.pos < n {
            let (v, a) = parse_packet(bits);
            version += v;
            val.push(a);
        }
    } else {
        for _ in 0..binary(bits.nibble(11)) {
            let (v, a) = parse_packet(bits);
            version += v;
            val.push(a);
        }
    }
    let n = match type_id {
        0 => val.into_iter().sum(),
        1 => val.into_iter().product(),
        2 => val.into_iter().min().unwrap(),
        3 => val.into_iter().max().unwrap(),
        5 => (val[0] > val[1]) as u64,
        6 => (val[0] < val[1]) as u64,
        7 => (val[0] == val[1]) as u64,
        _ => panic!("Bad type id: {}", type_id),
    };

    (version, n)
}
fn read_hex(input: &str) -> Packet {
    let mut vec = Vec::new();
    for c in input.chars() {
        let bits = match c {
            '0' => "0000",
            '1' => "0001",
            '2' => "0010",
            '3' => "0011",
            '4' => "0100",
            '5' => "0101",
            '6' => "0110",
            '7' => "0111",
            '8' => "1000",
            '9' => "1001",
            'A' => "1010",
            'B' => "1011",
            'C' => "1100",
            'D' => "1101",
            'E' => "1110",
            'F' => "1111",
            _ => panic!(),
        };
        vec.extend(bits.bytes().map(|b| (b - b'0') as u64));
    }
    Packet {
        hexbits: vec,
        pos: 0,
    }
}

fn main() {
    let input = include_str!("input.txt");
    let mut packets = read_hex(input.trim());
    let t = Instant::now();
    let a = parse_packet(&mut packets);
    let t1 = Instant::now() - t;
    println!("Part 1:{}, Part 2: {}, Time: {}µs", a.0, a.1, t1.as_micros());
    //solve(input.trim());
}
