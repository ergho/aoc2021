use std::collections::BTreeSet;
use std::str::FromStr;
use std::time::Instant;

struct Line {
    input: Vec<BTreeSet<char>>,
    output: Vec<BTreeSet<char>>,
}

impl FromStr for Line {
    type Err = &'static str;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (input, output) = s.trim().split_once(" | ").unwrap();
        let input = input
            .split_whitespace()
            .map(|segment| segment.chars().collect::<BTreeSet<_>>())
            .collect::<Vec<_>>();
        let output = output
            .split_whitespace()
            .map(|segment| segment.chars().collect::<BTreeSet<_>>())
            .collect::<Vec<_>>();
        Ok(Line { input, output })
    }
}

impl Line {

    fn fun(&self) -> i32 {
        let mut digits = vec![BTreeSet::new(); 10];
        let mut five: Vec<BTreeSet<char>> = vec![BTreeSet::new()];
        let mut six: Vec<BTreeSet<char>> = vec![BTreeSet::new()];
        for x in &self.input {
            match x.len() {
                // really ugly but oh well cba to fix it...
                2 => digits[1] = x.clone(),
                3 => digits[7] = x.clone(),
                4 => digits[4] = x.clone(),
                7 => digits[8] = x.clone(),
                5 => five.push(x.clone()),
                6 => six.push(x.clone()),
                _ => panic!(),
            }
        }
        for d in six {
            if !d.is_superset(&digits[1]) {
                digits[6] = d;
            } else if !d.is_superset(&digits[4]) {
                digits[0] = d;
            } else {
                digits[9] = d;
            }
        }

        for d in five {
            if d.is_subset(&digits[6]) {
                digits[5] = d;
            } else if d.is_subset(&digits[9]) {
                digits[3] = d;
            } else {
                digits[2] = d;
            }
        }

        self.output
            .iter()
            .map(|d| {
                digits
                    .iter()
                    .enumerate()
                    .find(|(_, x)| &d == x)
                    .unwrap()
                    .0
            })
            .fold(0, |acc, n| acc * 10 + n as i32)
    }
}

fn part1(input: &str) -> i32 {
    input
        .lines()
        .map(|l| {
            l.parse::<Line>()
                .unwrap()
                .output
                .iter()
                .filter(|d| matches!(d.len(), 2 | 4 | 3 | 7))
                .count()
        })
        .sum::<usize>() as i32
}

fn part2(input: &str) -> i32 {
    input
        .lines()
        .map(|l| l.parse::<Line>().unwrap().fun())
        .sum()
}

fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
    let part1 =part1(input);
    let t1 = Instant::now() - t;
    println!("part 1: {} (in {} µs)", part1, t1.as_micros());
    let t = Instant::now();
    let part2 =part2(input);
    let t1 = Instant::now() - t;
    println!("part 2: {} (in {} µs)", part2, t1.as_micros());
}
