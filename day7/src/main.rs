use std::time::Instant;

fn part1(input: &str) -> i32 {
    let crabs = input
        .trim()
        .split(',')
        .into_iter()
        .map(|i| i.parse::<i32>().unwrap())
        .collect::<Vec<_>>();
    let min = crabs.iter().min().unwrap();
    let max = crabs.iter().max().unwrap();

    (*min..=*max)
        .into_iter()
        .map(|i| crabs.iter().map(|crab| (*crab - i).abs()).sum())
        .min()
        .unwrap()
}

fn part2(input: &str) -> i32 {
    let crabs = input
        .trim()
        .split(',')
        .into_iter()
        .map(|i| i.parse::<i32>().unwrap())
        .collect::<Vec<_>>();
    let min = *crabs.iter().min().unwrap();
    let max = *crabs.iter().max().unwrap();
    (min..=max)
        .into_iter()
        .map(|i| {
            crabs
                .iter()
                .map(|crab| (*crab - i).abs())
                .map(|n| n * (n + 1) / 2)
                .sum()
        })
        .min()
        .unwrap()
}

fn part1_b(input: &str) -> i32 {
    let mut crabs = input
        .trim()
        .split(',')
        .map(|n| n.parse().unwrap())
        .collect::<Vec<i32>>();
    let mid = crabs.len() / 2;
    let mid = *crabs.select_nth_unstable(mid).1;
    crabs.iter().map(|n| (n - mid).abs()).sum()
}

fn part2_b(input: &str) -> i32 {
    let crabs = input
        .trim()
        .split(',')
        .map(|n| n.parse().unwrap())
        .collect::<Vec<i32>>();
    (crabs.iter().sum::<i32>() / crabs.len() as i32..)
        .take(2)
        .map(|t| {
            crabs
                .iter()
                .map(|d| {
                    let n = (d - t).abs();
                    n * (n + 1) / 2
                })
                .sum::<i32>()
        })
        .min()
        .unwrap()
}

fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
    let part1 = part1(input);
    let t1 = Instant::now() - t;
    println!("part 1: {} (in {} µs)", part1, t1.as_micros());
    let t = Instant::now();
    let part1_b = part1_b(input);
    let t1 = Instant::now() - t;
    println!("part 1 optimized: {} (in {} µs)", part1_b, t1.as_micros());
    let t = Instant::now();
    let part2 = part2(input);
    let t1 = Instant::now() - t;
    println!("part 2: {} (in {} µs)", part2, t1.as_micros());
    let t = Instant::now();
    let part2 = part2_b(input);
    let t1 = Instant::now() - t;
    println!("part 2 optimized: {} (in {} µs)", part2, t1.as_micros());
}

#[test]
fn day7() {
    let input = include_str!("test.txt");
    assert_eq!(part1(input), 37);
    assert_eq!(part2(input), 168);
}
