use std::time::Instant;
fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
    let first = solve_part1_a(&input);
    let t1 = Instant::now() - t;
    let t = Instant::now();
    let first_b = solve_part1_b(&input);
    let t1_b = Instant::now() - t;
    let t = Instant::now();
    let first_c = solve_part1_c(&input);
    let t1_c = Instant::now() - t;
    let t = Instant::now();
    let second = solve_part2_a(&input);
    let t2 = Instant::now() - t;
    let t = Instant::now();
    let second_b = solve_part2_b(&input);
    let t2_b = Instant::now() - t;
    let t = Instant::now();
    let second_c = solve_part2_c(&input);
    let t2_c = Instant::now() - t;
    println!(
        " Solution 1: {}, time in micros ({}μs)",
        first,
        t1.as_micros()
    );
    println!(
        " Solution 1 b: {}, time in micros ({}μs)",
        first_b,
        t1_b.as_micros()
    );
    println!(
        " Solution 1 c: {}, time in micros ({}μs)",
        first_c,
        t1_c.as_micros()
    );
    println!(
        " Solution 2: {}, time in micros ({}μs)",
        second,
        t2.as_micros()
    );
    println!(
        " Solution 2 b: {}, time in micros ({}μs)",
        second_b,
        t2_b.as_micros()
    );
    println!(
        " Solution 2 c: {}, time in micros ({}μs)",
        second_c,
        t2_c.as_micros()
    );
}

enum Direction {
    UP(i32),
    DOWN(i32),
    FORWARD(i32),
}

fn input_parsing(input: &str) -> Vec<Direction> {
    input
        .lines()
        .map(|l| {
            let mut d = l.split(' ');
            let ds = d.next().unwrap();
            let dist = d.next().unwrap().parse::<i32>().unwrap();
            match ds {
                "forward" => Direction::FORWARD(dist),
                "down" => Direction::DOWN(dist),
                "up" => Direction::UP(dist),
                _ => panic!(),
            }
        })
        .collect()
}

fn solve_part1_a(input: &str) -> i32 {
    let input = input_parsing(input);
    let mut x = 0;
    let mut y = 0;

    for d in input {
        match d {
            Direction::FORWARD(v) => x += v,
            Direction::DOWN(v) => y += v,
            Direction::UP(v) => y -= v,
        }
    }
    x * y
}

fn solve_part1_b(input: &str) -> i32 {
    let (d, x) = input
        .lines()
        .filter_map(|s| {
            s.split_once(' ')
                .map(|(i, n)| (i, n.parse::<i32>().unwrap()))
        })
        .fold((0, 0), |(d, x), (i, n)| match i {
            "forward" => (d, x + n),
            "down" => (d + n, x),
            "up" => (d - n, x),
            _ => panic!(),
        });
    d * x
}

fn solve_part1_c(input: &str) -> i32 {
    let (d, x) = input.lines().fold((0, 0), |(d, x), l| {
        let s = l.split_once(' ').unwrap();
        match (s.0, s.1.parse::<i32>().unwrap()) {
            ("forward", n) => (d, x + n),
            ("down", n) => (d + n, x),
            ("up", n) => (d - n, x),
            _ => panic!(),
        }
    });
    d * x
}

fn solve_part2_a(input: &str) -> i32 {
    let input = input_parsing(input);
    let mut x = 0;
    let mut y = 0;
    let mut a = 0;

    for d in input {
        match d {
            Direction::FORWARD(v) => {
                x += v;
                y += a * v;
            }
            Direction::DOWN(v) => {
                a += v;
            }
            Direction::UP(v) => {
                a -= v;
            }
        }
    }
    x * y
}

fn solve_part2_b(input: &str) -> i32 {
    let (d, x, _) = input
        .lines()
        .filter_map(|s| {
            s.split_once(' ')
                .map(|(i, n)| (i, n.parse::<i32>().unwrap()))
        })
        .fold((0, 0, 0), |(d, x, a), (i, n)| match i {
            "forward" => (d + a * n, x + n, a),
            "down" => (d, x, a + n),
            "up" => (d, x, a - n),
            _ => panic!(),
        });
    d * x
}

fn solve_part2_c(input: &str) -> i32 {
    let (d, x, _) = input.lines().fold((0, 0, 0), |(d, x, a), l| {
        let s = l.split_once(' ').unwrap();
        match (s.0, s.1.parse::<i32>().unwrap()) {
            ("forward", n) => (d + a * n, x + n, a),
            ("down", n) => (d, x, a + n),
            ("up", n) => (d, x, a - n),
            _ => panic!(),
        }
    });
    d * x
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(solve_part1_a(&input), 150);
    assert_eq!(solve_part1_b(&input), 150);
    assert_eq!(solve_part1_c(&input), 150);
    assert_eq!(solve_part2_a(&input), 900);
    assert_eq!(solve_part2_b(&input), 900);
    assert_eq!(solve_part2_c(&input), 900);
}
