use itertools::Itertools;
use std::collections::HashMap;

fn calc_freq(pairs: &HashMap<(char, char), usize>, last_char: char) -> usize {
    let mut counts: HashMap<char, usize> = HashMap::new();
    for (k, v) in pairs {
        (*counts.entry(k.0).or_default()) += v;
    }
    (*counts.entry(last_char).or_default()) += 1;
    counts.values().max().unwrap() - counts.values().min().unwrap()
}
fn solve(polymer: Vec<char>, rulemap: HashMap<(char, char), char>, steps: usize) -> usize {
    let last_char = *polymer.last().unwrap();
    let mut pairs: HashMap<(char, char), usize> = HashMap::new();
    for (a, b) in polymer.iter().tuple_windows() {
        (*pairs.entry((*a, *b)).or_default()) += 1;
    }

    for _ in 0..steps {
        let mut new_pairs: HashMap<(char, char), usize> = HashMap::new();
        for (&(a, b), v) in &pairs {
            let to_ins = *rulemap.get(&(a, b)).unwrap();
            (*new_pairs.entry((a, to_ins)).or_default()) += v;
            (*new_pairs.entry((to_ins, b)).or_default()) += v;
        }
        pairs = new_pairs
    }
    calc_freq(&pairs, last_char)
}
fn main() {
    let (polymer, rules) = include_str!("input.txt").split_once("\n\n").unwrap();
    let polymer = polymer.chars().collect_vec();
    let mut rulemap = HashMap::new();
    for line in rules.lines() {
        let (start, end) = line.split_once(" -> ").unwrap();
        let start = start.chars().collect_vec();
        rulemap.insert((start[0], start[1]), end.chars().next().unwrap());
    }
    let a = solve(polymer, rulemap, 40);
    println!("{}", a);
}
