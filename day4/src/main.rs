use std::time::Instant;

type Board = [(u8, bool); 25];

#[derive(Clone)]
struct BingoBoards {
    numbers: Vec<u8>,
    boards: Vec<Board>,
}

impl BingoBoards {
    fn parse_input(input: &str) -> BingoBoards {
        let mut sections = input.lines();

        let numbers = sections
            .next()
            .unwrap()
            .split(',')
            .map(|num| num.parse::<u8>().unwrap())
            .collect::<Vec<u8>>();
        let mut boards = Vec::new();

        while sections.next() == Some("") {
            let mut board = [(0, false); 25];
            for i in 0..5 {
                let mut section = sections
                    .next()
                    .unwrap()
                    .split_whitespace()
                    .map(|num| num.parse::<u8>());
                for j in 0..5 {
                    board[5 * i + j].0 = section.next().unwrap().unwrap();
                }
            }
            boards.push(board)
        }

        BingoBoards { numbers, boards }
    }
}
fn sum_unmarked(board: &Board) -> i32 {
    board
        .iter()
        .filter_map(|(val, mark)| (!mark).then(|| *val as i32))
        .sum()
}

fn check_board(board: &Board) -> bool {
    (0..5).any(|i| {
        let horizontal = (i * 5..i * 5 + 5).all(|i| board[i].1);
        let vertical = (i..21 + i).step_by(5).all(|i| board[i].1);
        horizontal || vertical
    })
}

fn mark_board(board: &mut Board, number: u8) {
    board.iter_mut().for_each(|x| {
        if x.0 == number {
            x.1 = true
        }
    });
}

fn part1(input: &str) -> i32 {
    let BingoBoards {
        numbers,
        mut boards,
    } = BingoBoards::parse_input(input);

    for num in numbers {
        for board in boards.iter_mut() {
            mark_board(board, num);
            if check_board(&board) {
                return sum_unmarked(&board) * num as i32;
            }
        }
    }
    panic!();
}

fn part2(input: &str) -> i32 {
    let BingoBoards {
        numbers,
        mut boards,
    } = BingoBoards::parse_input(input);

    let mut boards_won = vec![false; boards.len()];
    for num in numbers {
        for (i, board) in boards.iter_mut().enumerate() {
            if boards_won[i] {
                continue;
            }
            mark_board(board, num);

            if check_board(&board) {
                boards_won[i] = true;
                if boards_won.iter().all(|x| *x) {
                    return sum_unmarked(&board) * num as i32;
                }
            }
        }
    }
    panic!();
}

fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
    let part1 = part1(input);
    let t1 = Instant::now() - t;
    println!("part 1: {} (in {} µs)", part1, t1.as_micros());

    let t = Instant::now();
    let part2 = part2(input);
    let t1 = Instant::now() - t;
    println!("part 2: {} (in {} µs)", part2, t1.as_micros());
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(part1(input), 4512);
    assert_eq!(part2(input), 1924);
}
