use std::time::Instant;
use std::collections::HashSet;

fn fold_grid(points: &HashSet<(i16, i16)>, (dir, pos): (char, i16)) -> HashSet<(i16, i16)> {
    points
        .iter()
        .map(|&(x, y)| match (dir, x, y) {
            ('x', x, y) if x < pos => (x, y),
            ('x', x, y) => (pos * 2 - x, y),
            ('y', x, y) if y < pos => (x, y),
            ('y', x, y) => (x, pos * 2 - y),
            _ => unreachable!(),
        })
        .collect()
}

fn part2(grid: HashSet<(i16, i16)>, folds: &[(char, i16)]) {
    let fg = folds.iter().fold(grid, | grid,&fold| fold_grid(&grid, fold));
    for y in 0..6 {
        for x in 0..40 {
            let c = if fg.contains(&(x,y)) {'#'} else {' '};
            print!("{}", c);
        }
        println!("");
    }
}
fn main() {
    let (coords, folds) = include_str!("input.txt").split_once("\n\n").unwrap();
    let grid = coords
        .lines()
        .map(|l| {
            let (x, y) = l.split_once(',').unwrap();
            (x.parse::<i16>().unwrap(), y.parse::<i16>().unwrap())
        })
        .collect::<HashSet<_>>();
    let folds = folds
        .lines()
        .map(|l| {
            let ins = l.split_whitespace().last().unwrap();
            let (dir, pos) = ins.split_once('=').unwrap();
            (dir.as_bytes()[0] as char, pos.parse::<i16>().unwrap())
        })
        .collect::<Vec<_>>();
    let t = Instant::now();
    let p1 = fold_grid(&grid, folds[0]).len();
    let t1 = Instant::now() - t;
    println!(
        " Solution 1: {}, time in micros ({}μs)",
        p1,
        t1.as_micros()
    );
    let t = Instant::now();
    part2(grid, &folds);
    let t1 = Instant::now() - t;
    println!(
        " Solution 2, time in micros ({}μs)",
        t1.as_micros()
    );
}
