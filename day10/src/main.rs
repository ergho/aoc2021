use std::time::Instant;

fn main() {
    let t = Instant::now();
    let input = include_str!("input.txt");
    let mut ans = 0;
    let mut scores: Vec<_> = Vec::new();

    for line in input.lines() {
        let mut bad = false;
        let mut stack = Vec::new();
        for c in line.trim().chars() {
            match c {
                '(' | '[' | '{' | '<' => stack.push(c),
                ')' => {
                    if stack.last().unwrap() != &'(' {
                        ans += 3;
                        bad = true;
                        break;
                    } else {
                        stack.pop();
                    }
                }
                ']' => {
                    if stack.last().unwrap() != &'[' {
                        ans += 57;
                        bad = true;
                        break;
                    } else {
                        stack.pop();
                    }
                }
                '}' => {
                    if stack.last().unwrap() != &'{' {
                        ans += 1197;
                        bad = true;
                        break;
                    } else {
                        stack.pop();
                    }
                }
                '>' => {
                    if stack.last().unwrap() != &'<' {
                        ans += 25137;
                        bad = true;
                        break;
                    } else {
                        stack.pop();
                    }
                }
                _ => {}
            }
        }
        if !bad {
            let mut score = 0u64;
            for c in stack.into_iter().rev() {
                match c {
                    '(' => score = score * 5 + 1,
                    '[' => score = score * 5 + 2,
                    '{' => score = score * 5 + 3,
                    '<' => score = score * 5 + 4,
                    _ => panic!(),
                }
            }
            scores.push(score);
        }
    }
    scores.sort();
    let scores = scores[scores.len() / 2];
    let t1 = Instant::now() - t;
    println!("{}", ans);
    println!("{}", scores);
    println!("day 10 solution (in {} µs)", t1.as_micros());
}
