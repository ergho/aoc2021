use std::{collections::{HashMap, HashSet, VecDeque}, time::Instant};
type CaveSystem = HashMap<Cave, HashSet<Cave>>;

#[derive(Debug, Clone, PartialEq, Eq, Hash)]
enum Cave {
    Something(u32),
    Start,
    End,
}

fn to_cave<'a>(key_map: &mut HashMap<&'a str, u32>, max_val: &mut u32, input: &'a str) -> Cave {
    match input {
        "start" => Cave::Start,
        "end" => Cave::End,
        _ => {
            let lower = input.chars().next().unwrap().is_lowercase();
            let key = match key_map.get(input) {
                Some(k) => *k,
                None => {
                    *max_val <<= 1;
                    let key = *max_val + if lower { 1 } else { 0 };
                    key_map.insert(input, key);
                    key
                }
            };
            Cave::Something(key)
        }
    }
}

fn parse(input: &str) -> CaveSystem {
    let mut map: CaveSystem = HashMap::new();
    let mut keys: HashMap<&str, u32> = HashMap::new();
    let mut max_val = 1;

    for line in input.lines() {
        let (a, b) = line.split_once('-').unwrap();
        let cave_a = to_cave(&mut keys, &mut max_val, a);
        let cave_b = to_cave(&mut keys, &mut max_val, b);
        let entry1 = map.entry(cave_a.clone()).or_insert(HashSet::new());
        entry1.insert(cave_b.clone());
        let entry2 = map.entry(cave_b.clone()).or_insert(HashSet::new());
        entry2.insert(cave_a.clone());
    }

    map
}

fn solve(cavesystem: &CaveSystem, visit_twice: bool) -> usize {
    let visited_twice = visit_twice;
    let mut queue = VecDeque::new();
    queue.push_back((&Cave::Start, visited_twice, 0));
    let mut sum = 0;

    while let Some((path, vt, visited)) = queue.pop_front() {
        let current_cave = path;
        let caves = cavesystem.get(&current_cave).unwrap();
        for cave in caves {
            match cave {
                Cave::Something(k) => {
                    if k & 1 == 1 {
                        let uk = k ^ 1;
                        if visited & uk != uk {
                            let path = visited | uk;
                            queue.push_back((cave, vt, path));
                        } else if !vt {
                            let path = visited | uk;
                            queue.push_back((&cave, true, path));
                        }
                    } else {
                        queue.push_back((&cave, vt, visited));
                    }
                }
                Cave::End => {
                    sum += 1;
                }
                Cave::Start => {}
            }
        }
    }
    sum
}

fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
   let d12 = solve(&parse(input), false);
    let t1 = Instant::now() - t;
    println!("part 2 {} in time {}", d12, t1.as_micros());
}
