use std::{
    collections::{HashMap, HashSet, VecDeque},
    str::FromStr,
    time::Instant,
};

type Pos = (i32, i32);

#[derive(Debug)]
struct Grid(HashMap<Pos, u32>);

impl FromStr for Grid {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let grid = s
            .lines()
            .enumerate()
            .flat_map(|(i, line)| {
                line.chars().enumerate().map(move |(x, height)| {
                    let height = height.to_digit(10).unwrap();
                    ((x as i32, i as i32), height)
                })
            })
            .collect();
        Ok(Self(grid))
    }
}

impl Grid {
    fn get(&self, pos: Pos) -> Option<u32> {
        self.0.get(&pos).copied()
    }

    fn check_low_points(&self, pos: Pos) -> bool {
        let point = self.get(pos).unwrap();
        self.get((pos.0, pos.1 - 1))
            .into_iter()
            .chain(self.get((pos.0 - 1, pos.1)))
            .chain(self.get((pos.0 + 1, pos.1)))
            .chain(self.get((pos.0, pos.1 + 1)))
            .all(|p| p > point)
    }

    fn get_lowest_points(&self) -> impl Iterator<Item = (&Pos, &u32)> {
        self.0.iter().filter(|(k, _)| self.check_low_points(**k))
    }

    fn get_basins(&self, pos: Pos) -> usize {
        let mut visited = HashSet::new();
        let mut queue = VecDeque::new();
        queue.push_back(pos);
        while let Some(pos) = queue.pop_front() {
            if let Some(0..=8) = self.get(pos) {
                if visited.insert(pos) {
                    queue.push_back((pos.0, pos.1 - 1));
                    queue.push_back((pos.0 - 1, pos.1));
                    queue.push_back((pos.0 + 1, pos.1));
                    queue.push_back((pos.0, pos.1 + 1));
                }
            }
        }
        visited.len()
    }
}
fn part1(input: &str) -> u32 {
    let grid = input.parse::<Grid>().unwrap();
    grid.get_lowest_points().map(|(_, v)| v + 1).sum()
}

fn part2(input: &str) -> usize {
    let grid = input.parse::<Grid>().unwrap();
    let mut basins: Vec<usize> = grid
        .get_lowest_points()
        .map(|(k, _)| grid.get_basins(*k))
        .collect();

    basins.sort();
    basins.iter().rev().take(3).product::<usize>()
}

fn main() {
    let input = include_str!("input.txt");

    let t = Instant::now();
    let part1 = part1(input);
    let t1 = Instant::now().duration_since(t);
    println!("part 1: {} (in {} µs)", part1, t1.as_micros());
    let t = Instant::now();
    let part2 = part2(input);
    let t1 = Instant::now().duration_since(t);
    println!("part 2: {} (in {} µs)", part2, t1.as_micros());
}
