use std::{collections::HashMap, str::FromStr, time::Instant};

#[derive(Debug, PartialEq, Eq, Clone, Copy, Hash)]
struct Point {
    x: i16,
    y: i16,
}

impl FromStr for Point {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (x, y) = s.split_once(',').unwrap();
        let x = x.parse::<i16>().unwrap();
        let y = y.parse::<i16>().unwrap();
        Ok(Self { x, y })
    }
}

struct Line {
    start: Point,
    end: Point,
}

impl FromStr for Line {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let (start, end) = s.split_once(" -> ").unwrap();
        let start = start.parse::<Point>()?;
        let end = end.parse::<Point>()?;
        Ok(Self { start, end })
    }
}

impl Line {
    fn list_of_points(&self, allow_diag: bool) -> Vec<Point> {
        let mut points = Vec::new();
        let mut dy = self.end.y - self.start.y;
        let mut dx = self.end.x - self.start.x;

        // dont check for diags if we aren,t by exiting early, accidentally did initially woops...
        if !allow_diag && (dx != 0 && dy != 0) {
            return points;
        }
        while dx != 0 || dy != 0 {
            let x = self.start.x + dx;
            let y = self.start.y + dy;
            points.push(Point { x, y });
            match dx {
                x if x > 0 => dx -= 1,
                x if x < 0 => dx += 1,
                0 => {}
                _ => panic!(),
            }
            match dy {
                y if y > 0 => dy -= 1,
                y if y < 0 => dy += 1,
                0 => {}
                _ => panic!(),
            }
        }

        points.push(self.start);

        points
    }
}

fn find_intersections(input: &str, allow_diag: bool) -> usize {
    let input: Vec<Line> = input.lines().map(|x| x.parse::<Line>().unwrap()).collect();

    let mut map = HashMap::new();
    for line in input {
        for point in line.list_of_points(allow_diag) {
            if let Some(v) = map.get_mut(&point) {
                *v += 1;
            } else {
                map.insert(point, 1);
            }
        }
    }
    map.iter().filter(|(_pos, val)| val > &&1).count()
}

fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
    let part1 = find_intersections(input, false);
    let t1 = Instant::now() - t;
    println!("part 1: {} (in {} µs)", part1, t1.as_micros());
    let t = Instant::now();
    let part2 = find_intersections(input, true);
    let t1 = Instant::now() - t;
    println!("part 2: {} (in {} µs)", part2, t1.as_micros());
}

#[cfg(test)]
mod tests {
    use super::*;
    #[test]
    fn test_both() {
        let input = include_str!("test.txt");
        assert_eq!(find_intersections(input, false), 5);
        assert_eq!(find_intersections(input, true), 12);
    }

    #[test]
    fn line_to_points() {
        let line = "1,5 -> 1,7".parse::<Line>().unwrap();
        assert_eq!(
            line.list_of_points(true),
            vec![
                Point { x: 1, y: 7 },
                Point { x: 1, y: 6 },
                Point { x: 1, y: 5 }
            ]
        );
        let line = "5,6 -> 3,8".parse::<Line>().unwrap();
        assert_eq!(
            line.list_of_points(true),
            vec![
                Point { x: 3, y: 8 },
                Point { x: 4, y: 7 },
                Point { x: 5, y: 6 }
            ]
        );
        let line = "5,6 -> 9,6".parse::<Line>().unwrap();
        assert_eq!(
            line.list_of_points(true),
            vec![
                Point { x: 9, y: 6 },
                Point { x: 8, y: 6 },
                Point { x: 7, y: 6 },
                Point { x: 6, y: 6 },
                Point { x: 5, y: 6 },
            ]
        );
        let line = "5,6 -> 3,8".parse::<Line>().unwrap();
        assert_eq!(line.list_of_points(false), vec![]);
    }
}
