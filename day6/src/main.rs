use std::time::Instant;

fn fish_growth(fishes: Vec<u8>, days: usize) -> usize {
    let mut fish_count = [0; 9];
    for fish in fishes {
        fish_count[fish as usize] += 1;
    }

    for _day in 0..days {
        fish_count.rotate_left(1);
        fish_count[6] += fish_count[8];
    }

    fish_count.iter().sum()
}

fn solve(input: &str, days: usize) -> usize {
    let fish = input
        .trim()
        .split(',')
        .filter_map(|x| x.parse::<u8>().ok())
        .collect::<Vec<u8>>();

    fish_growth(fish, days)
}

fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
    let part1 = solve(input, 80);
    let t1 = Instant::now() - t;
    println!("part 1: {} (in {} µs)", part1, t1.as_micros());
    let t = Instant::now();
    let part2 = solve(input, 256);
    let t1 = Instant::now() - t;
    println!("part 2: {} (in {} µs)", part2, t1.as_micros());
}

#[test]
fn day6() {
    let input = vec![3, 4, 3, 1, 2];
    assert_eq!(fish_growth(input.clone(), 18), 26);
    assert_eq!(fish_growth(input, 80), 5934);
}
