use std::collections::{HashMap, VecDeque};

struct Cave {
    is_big: bool,
    connections: Vec<String>,
}

impl Cave {
    fn new(cave_name: &str) -> Self {
        Cave {
            is_big: cave_name.chars().all(|c| c.is_uppercase()),
            connections: Vec::new(),
        }
    }
}

fn solve(caves: &HashMap<String, Cave>, multiple_visits: bool) -> usize {
    let mut path_count = 0;
    let mut queue = VecDeque::new();
    queue.push_back((vec![String::from("start")], false));

    while let Some((path, mut visited)) = queue.pop_front() {
        let current_path = path.last().unwrap();
        let cave = caves.get(current_path).unwrap();
        if current_path == "end" {
            path_count += 1;
            continue;
        }
        if !cave.is_big && path.iter().filter(|&e| e == current_path).count() > 1 {
            if !multiple_visits || visited || current_path == "start" {
                continue;
            } else {
                visited = true;
            }
        }
        for connection in &cave.connections {
            let mut new_path = path.clone();
            new_path.push(connection.clone());
            queue.push_back((new_path, visited));
        }
    }

    path_count
}
fn main() {
    let input = include_str!("input.txt");
    let mut caves: HashMap<String, Cave> = HashMap::new();
    for line in input.lines() {
        let (a,b) = line.split_once('-').unwrap();
        {
            let a = caves.entry(a.to_owned()).or_insert(Cave::new(a));
            a.connections.push(b.to_owned());
        }
        {
            let b = caves.entry(b.to_owned()).or_insert(Cave::new(b));
            b.connections.push(a.to_owned());
        }
    }
    println!("part 1: {}", solve(&caves, false));
    println!("part 2: {}", solve(&caves, true));
}
