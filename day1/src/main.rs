use std::time::Instant;
fn main() {
    let input = include_str!("input.txt");
    let t = Instant::now();
    let first = solve_part1_a(input);
    let t1 = Instant::now() - t;
    let t = Instant::now();
    let first_b = solve_part1_b(input);
    let t1_b = Instant::now() - t;
    let t = Instant::now();
    let first_c = solve_part1_c(input);
    let t1_c = Instant::now() - t;
    let t = Instant::now();
    let second = solve_part2_a(input);
    let t2 = Instant::now() - t;
    let t = Instant::now();
    let second_b = solve_part2_b(input);
    let t2_b = Instant::now() - t;
    let t = Instant::now();
    let second_c = solve_part2_c(input);
    let t2_c = Instant::now() - t;
    println!(
        " Solution 1: {}, time in micros ({}μs)",
        first,
        t1.as_micros()
    );
    println!(
        " Solution 1 b: {}, time in micros ({}μs)",
        first_b,
        t1_b.as_micros()
    );
    println!(
        " Solution 1 c: {}, time in micros ({}μs)",
        first_c,
        t1_c.as_micros()
    );
    println!(
        " Solution 2: {}, time in micros ({}μs)",
        second,
        t2.as_micros()
    );
    println!(
        " Solution 2 b: {}, time in micros ({}μs)",
        second_b,
        t2_b.as_micros()
    );
    println!(
        " Solution 2 c: {}, time in micros ({}μs)",
        second_c,
        t2_c.as_micros()
    );
}

fn solve_part1_a(input: &str) -> i32 {
    let depths: Vec<_> = input.lines().map(|l| l.parse::<i32>().unwrap()).collect();
    let mut counts = 0;
    for x in 1..depths.len() {
        if depths[x] > depths[x - 1] {
            counts += 1;
        }
    }
    counts
}

fn solve_part1_b(input: &str) -> i32 {
    input
        .lines()
        .map(|l| l.parse::<i32>().unwrap())
        .fold((0, i32::MAX), |(c, p), n| {
            (if n > p { c + 1 } else { c }, n)
        })
        .0
}

fn solve_part1_c(input: &str) -> i32 {
    let depths: Vec<i32> = input.lines().map(|l| l.parse::<i32>().unwrap()).collect();
    depths.windows(2).filter(|w| w[0] < w[1]).count() as i32
}

fn solve_part2_a(input: &str) -> i32 {
    let depths: Vec<_> = input.lines().map(|l| l.parse::<i32>().unwrap()).collect();
    let mut counts = 0;
    for x in 3..depths.len() {
        let first = depths[x - 1] + depths[x - 2] + depths[x - 3];
        let second = depths[x] + depths[x - 1] + depths[x - 2];
        if second > first {
            counts += 1;
        }
    }
    counts
}

fn solve_part2_b(input: &str) -> i32 {
    input
        .lines()
        .map(|s| s.parse::<i32>().unwrap())
        .fold((0, i32::MAX, None, None), |(sum, prev, a, b), c| {
            a.and_then(|a| {
                b.and_then(|b| {
                    Some((
                        if a + b + c > prev { sum + 1 } else { sum },
                        a + b + c,
                        Some(b),
                        Some(c),
                    ))
                })
            })
            .unwrap_or((sum, prev, b, Some(c)))
        })
        .0
}

fn solve_part2_c(input: &str) -> i32 {
    let depths: Vec<i32> = input.lines().map(|l| l.parse::<i32>().unwrap()).collect();
    depths.windows(4).filter(|w| w[3] > w[0]).count() as i32
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(solve_part1_a(input), 7);
    assert_eq!(solve_part1_b(input), 7);
    assert_eq!(solve_part1_c(input), 7);
    assert_eq!(solve_part2_a(input), 5);
    assert_eq!(solve_part2_b(input), 5);
    assert_eq!(solve_part2_c(input), 5);
}
