use std::collections::BinaryHeap;
use std::time::Instant;

fn solve(map: Vec<Vec<i32>>) -> i32 {
    let mut queue = BinaryHeap::new();
    let width = map.len();
    let height = map[0].len();
    let goal = (height - 1, width - 1);
    let mut costs = vec![vec![i32::MAX; width]; height];
    queue.push((0, 0, 0));
    while let Some((cost, x, y)) = queue.pop() {
        if (x, y) == goal {
            return -cost;
        };
        if -cost > costs[x][y] {
            continue;
        }
        for (x1, y1) in [(x - 1, y), (x + 1, y), (x, y - 1), (x, y + 1)] {
            let next_cost = match map.get(x1).and_then(|row| row.get(y1)) {
                Some(r) => -cost + r,
                None => continue,
            };
            if next_cost < costs[x1][y1] {
                queue.push((-next_cost, x1, y1));
                costs[x1][y1] = next_cost;
            }
        }
    }
    panic!();
}
fn main() {
    let map: Vec<Vec<i32>> = include_str!("input.txt")
        .lines()
        .map(|l| l.chars().map(|c| (c as u8 - b'0') as i32).collect())
        .collect();
    let large_map = (0..(5 * map.len()))
        .map(|x| {
            (0..(5 * map[0].len()))
                .map(|y| {
                    let xlevel = (x / map.len()) as i32;
                    let ylevel = (y / map[0].len()) as i32;
                    let cost = map[x % map.len()][y % map[0].len()] + xlevel + ylevel;
                    if cost < 10 {
                        cost
                    } else {
                        cost - 9
                    }
                })
                .collect::<Vec<_>>()
        })
        .collect::<Vec<_>>();

    let t = Instant::now();
    let p1 = solve(map);
    let t1 = Instant::now() - t;
    println!("part 1 {} in time {}", p1, t1.as_micros());
    let t = Instant::now();
    let p2 = solve(large_map);
    let t1 = Instant::now() - t;
    println!("part 2 {} in time {}", p2, t1.as_micros());
}
