use std::{
    collections::{HashMap, HashSet, VecDeque},
    str::FromStr,
};

type Pos = (i8, i8);
#[derive(Debug)]
struct Grid {
    board: HashMap<Pos, u8>,
    marked: HashSet<Pos>,
    queue: VecDeque<Pos>,
    flashes: usize,
}

impl FromStr for Grid {
    type Err = &'static str;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let board = s
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars().enumerate().map(move |(x, octupus)| {
                    let octupus = octupus.to_digit(10).unwrap();
                    ((x as i8, y as i8), octupus as u8)
                })
            })
            .collect();

        Ok(Self {
            board,
            marked: HashSet::new(),
            queue: VecDeque::new(),
            flashes: 0,
        })
    }
}

impl Grid {
    fn solve(&mut self, days: usize, coalesce: bool) -> usize {
        for c in 1..=days {
            for x in 0..10 {
                for y in 0..10 {
                    self.update_pos((x, y));
                    if self.get((x, y)) > Some(9) {
                        self.queue.push_back((x, y));
                    }
                }
            }
            if self.update_neighbours(coalesce) {
                return c;
            }
        }
        self.flashes
    }
    fn update_pos(&mut self, pos: Pos) {
        *self.board.get_mut(&pos).unwrap() += 1;
    }
    fn get(&self, pos: Pos) -> Option<u8> {
        self.board.get(&pos).copied()
    }
    fn reset_pos(&mut self, pos: Pos) {
        self.board.insert(pos, 0);
    }
    fn update_neighbours(&mut self, coalesce: bool) -> bool {
        let mut flashes = 0;
        while let Some(pos) = self.queue.pop_front() {
            if let Some(0..=10) = self.get(pos) {
                if !self.marked.contains(&pos) {
                    self.update_pos(pos);
                    if self.get(pos) > Some(9) {
                        self.reset_pos(pos);
                        flashes += 1;
                        self.marked.insert(pos);
                        self.queue.push_back((pos.0 - 1, pos.1 - 1));
                        self.queue.push_back((pos.0 - 1, pos.1));
                        self.queue.push_back((pos.0 - 1, pos.1 + 1));
                        self.queue.push_back((pos.0, pos.1 - 1));
                        self.queue.push_back((pos.0, pos.1 + 1));
                        self.queue.push_back((pos.0 + 1, pos.1 - 1));
                        self.queue.push_back((pos.0 + 1, pos.1));
                        self.queue.push_back((pos.0 + 1, pos.1 + 1));
                    }
                }
            }
        }

        if flashes == 100 && coalesce {
            return true;
        }
        self.queue.clear();
        self.marked.clear();

        self.flashes += flashes;
        false
    }
}

fn main() {
    let input = include_str!("input.txt");
    let mut grid = input.parse::<Grid>().unwrap();
    let b = grid.solve(100, false);
    println!("{}", b);
}
