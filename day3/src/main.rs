#[derive(Debug)]
struct Part1 {
    zeroes: [i32; 12],
    ones: [i32; 12],
    max: usize,
}

impl Part1 {
    fn new() -> Part1 {
        Part1 {
            zeroes: [0; 12],
            ones: [0; 12],
            max: 0,
        }
    }

    fn parse_input(&mut self, input: &str) {
        let (zeroes, ones, max) = input
            .lines()
            .map(|num| (i32::from_str_radix(num, 2).unwrap(), num.len()))
            .fold(
                ([0; 12], [0; 12], 0),
                |(mut zeroes, mut ones, m), (bits, len)| {
                    for i in 0..len {
                        if (bits >> i) & 1 == 1 {
                            ones[i] += 1;
                        } else {
                            zeroes[i] += 1;
                        }
                    }
                    (zeroes, ones, m.max(len))
                },
            );

        self.zeroes = zeroes;
        self.ones = ones;
        self.max = max;
    }
    fn solve(&self) -> i32 {
        let (gamma, epsilon) = (0..self.max).fold((0, 0), |(gamma, epsilon), i| {
            if self.zeroes[i] < self.ones[i] {
                (gamma | (1 << i), epsilon)
            } else {
                (gamma, epsilon | (1 << i))
            }
        });
        gamma * epsilon
    }
}

struct Part2 {
    nums1: Vec<i32>,
    nums2: Vec<i32>,
    max: usize,
}

impl Part2 {
    fn new() -> Part2 {
        Part2 {
            nums1: vec![],
            nums2: vec![],
            max: 0,
        }
    }

    fn parse_input(&mut self, input: &str) {
        let (nums, max) = input.lines().fold((Vec::new(), 0), |(mut nums, max), s| {
            nums.push(i32::from_str_radix(s, 2).unwrap());
            (nums, max.max(s.len()))
        });
        self.nums1 = nums.clone();
        self.nums2 = nums;
        self.max = max;
    }
    fn solve(&mut self) -> i32 {
        let count = |nums: &[i32], bit| {
            nums.iter().fold((0, 0), |(zeroes, ones), n| {
                if (n & bit) == bit {
                    (zeroes, ones + 1)
                } else {
                    (zeroes + 1, ones)
                }
            })
        };
        for i in 1..=self.max {
            let i = self.max - i;
            let bit = 1 << i;
            if self.nums1.len() > 1 {
                let (zeros, ones) = count(&self.nums1, bit);
                let want = if ones >= zeros { bit } else { 0 };
                self.nums1 = self
                    .nums1
                    .iter()
                    .copied()
                    .filter(|n| n & bit == want)
                    .collect();
            }
            if self.nums2.len() > 1 {
                let (zeros, ones) = count(&self.nums2, bit);
                let want = if ones < zeros { bit } else { 0 };
                self.nums2 = self
                    .nums2
                    .iter()
                    .copied()
                    .filter(|n| n & bit == want)
                    .collect();
            }
        }

        self.nums1[0] * self.nums2[0]
    }
}

fn part1(input: &str) -> i32 {
    let mut part1 = Part1::new();
    part1.parse_input(input);
    part1.solve()
}

fn part2(input: &str) -> i32 {
    let mut part2 = Part2::new();
    part2.parse_input(input);
    part2.solve()
}

fn main() {
    use std::time::Instant;

    let input = include_str!("input.txt");

    let t = Instant::now();
    let solution1 = part1(input);
    let time1 = Instant::now() - t;

    let t = Instant::now();
    let solution2 = part2(input);
    let time2 = Instant::now() - t;

    println!("PART 1: {} ({} μs)", solution1, time1.as_micros());
    println!("PART 2: {} ({} μs)", solution2, time2.as_micros());
}

#[test]
fn test() {
    let input = include_str!("test.txt");
    assert_eq!(part1(input), 198);
    assert_eq!(part2(input), 230);
}
